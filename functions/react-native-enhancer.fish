function react-native-enhancer -a cmd --description='React native enhancer functions'
    if count $argv > 0
        if test $cmd = 'debugger'
          open "rndebugger://set-debugger-loc?host=localhost&port=8081"	
        end
        if test $cmd = 'patch'
          sed -i '' 's/startsWith/includes/g' node_modules/react-native/local-cli/runIOS/findMatchingSimulator.js
        end
        if test $cmd = 'shake'
            adb reverse tcp:8081 tcp:8081
            adb shell input keyevent 82
        end
        if test $cmd = 'sshot'
          adb shell screencap -p /sdcard/screen.png
          adb pull /sdcard/screen.png
          adb shell rm /sdcard/screen.png
        end
    else
        __react-native-enhancer-show-help
    end
end

function __react-native-enhancer-show-help
    echo (string split . (basename (status filename)))[1]
    echo "debugger"
    echo \t"starts react-native-debugger if installed: https://github.com/jhen0409/react-native-debugger"
    echo "patch"
    echo \t"patches your findMatchingSimulator.js for iOS. Due to March 2019 XCode update"
    echo "shake"
    echo \t"Makes and adb reverse and opens your android development menu"
    echo "sshot"
    echo \t"Takes a screenshot of your Android, saves to the current directory with the name 'screen.png' and deletes it from the device"
end

